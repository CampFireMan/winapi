const context = require('../inc/context');

module.exports = {
	getIdFromKey: (entity, API_key, callback) => {
		if (API_key.length != 50) {
			return callback(new Error("Invalid API_key"));
		}

		entity.findOne({
			where: {
	    	API_key: API_key 
	  	}
	  }).then(station => {
	  	if (station == null) {
	  		return callback(new Error("API_key not found"), null);
	  	} else {
	  		return callback(null, station.id);
	  	}
		}).catch(err => {
			console.log(err);
			return callback(new Error(err), null);
		});
	},

	retrieveInfo: (entity, id, callback) => {
		entity.findAll({
			where: {
				created_by: id
			}
		}).then(stations => {
			const data = stations.map(el => el.get({ plain: true }));
			if (stations) {
				return callback(null, data);
			} else {
				return callback(new Error("No entity information found"), null);
			}
		}).catch(err => {
			console.log(err);
			return callback(new Error(err), null);
		})
	},

	context: (req) => {
		return new context(req);
	}
}