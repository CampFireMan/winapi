module.exports = {
    E: function (msg) {
      return {
        error: true,
        msg: msg
      };
    },

    S: function (msg) {
      return {
        error: false,
        msg: msg
      };
    }
}