module.exports = class Context {
	constructor (req) {
		this.context = {user: req.user, data: {}, msg: {}};
		if (req.user.rights > 1) {
			this.context['admin'] = true;
		}
	}

	E (msg) {
		this.context.msg['error'] = msg;
		return this.context;
	}

	S (msg) {
		this.context.msg['success'] = msg;
		return this.context;
	}

	get () {
		return this.context;
	}
}