const op = require("../inc/operations.js");
const nanoid = require('nanoid');
const context = require('../inc/context');

module.exports = function (app, db) {
	result = {};
	result.dashboard = (req, res) => {
		var context = op.context(req).get();
		op.retrieveInfo(db.weather_stations, context.user.uid, (err, data) => {
			if (err || data.length == 0) {
				context['data']['weather_stations'] = null;
			} else {
				context['data']['weather_stations'] = data;
			}
			return res.render('dashboard/dashboard', context);
		})
	};

	result.registerWeatherStation = (req, res) => {
		var context = op.context(req);
		db.weather_stations.findOne({
			where: {
				name: req.body.name
			}
		}).then(weather_station => {
			if (weather_station) {
				return res.render('dashboard/register-weather-station', context.E('Der Name der Wetterstation existiert bereits'));
			}

			db.weather_stations.create({
				name: req.body.name,
				lat: req.body.lat,
				long: req.body.long,
				API_key: nanoid(50),
				created_by: req.user.uid
			}).then(station => {
				return res.render('dashboard/register-weather-station', context.S('Wetterstation erfolgreich erstellt'));
			}).catch(err => {
				console.log(err);
				return res.render('dashboard/register-weather-station', context.E('Es ist ein Fehler aufgetreten'));
			})
		});
	}

	return result;
}