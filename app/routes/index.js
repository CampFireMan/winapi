// /routes/index.js

const api = require('./api');
const auth = require('./auth');
const dashboard = require('./dashboard');
const datacenter = require('./datacenter');

module.exports = function(app, sequelize, db, passport) {
	app.get('/', (req, res) => {
		res.render('index');
	})
	
	api(app, sequelize, db);
	auth(app, passport);
	dashboard(app, db);
	datacenter(app, db);
}