const reply = require("../inc/reply.js");
const op = require("../inc/operations.js");
const nanoid = require('nanoid');

module.exports = function(app, db) {
	app.get('/test', (req, res) => {
		res.render('test');
	})
};