const op = require("../inc/operations.js");

module.exports = function(app, db) {
	const dashboard = require('../controllers/dashboardcontroller.js')(app, db);
	app.get('/dashboard/weather_station/register', isLoggedIn, (req, res) => {
		var context = op.context(req);
		res.render('dashboard/register-weather-station', context.get());
	});

	// insert weather station into DB
	app.post('/dashboard/weather_station/register', isLoggedIn, dashboard.registerWeatherStation);

	app.get('/dashboard', isLoggedIn, dashboard.dashboard);

	function isLoggedIn(req, res, next) {
		if (req.isAuthenticated())
			return next();
		res.redirect('/login');
	}

	app.get('/generate', (req, res) => {
		var timestamp = new Date();
		for (let i = 0; i < 100; i++) {
			timestamp.setMinutes(timestamp.getMinutes() + 10);
			test(timestamp.toISOString(), 16);
		}
	});

	function test (timestamp, station) {
		db.weather_data.create({
			station_id: station,
			temp: random(10, 30),
			humidity: random(50, 90),
			pressure: random(950, 1050),
			wind_speed: random(0, 10),
			wind_direction: random(0, 359),
			timestamp: timestamp
		})
	}

	function random (low, high) {
		return Math.floor(Math.random() * (high -low) + low);
	}
}