// /routes/note_routes.js

const reply = require("../inc/reply.js");
const op = require("../inc/operations.js");
const nanoid = require('nanoid');

module.exports = function(app, sequelize, db) {

	// insert weather data into DB
	app.post('/api/weather_station', (req, res) => {
		op.getIdFromKey(db.weather_stations, req.body.API_key, (err, id) => {
			if (err) {
				return res.send(reply.E(err));
			}
			db.weather_data.create({
	  		station_id: id,
	  		temp: parseFloat(req.body.temp),
	  		pressure: parseFloat(req.body.pressure),
	  		windspeed: parseFloat(req.body.windspeed),
	  		timestamp: req.body.timestamp
	  	}).then(weather_data => {
	  		return res.send(reply.S('Insert successful'));
	  	}).catch(err => {
	  		console.log(err);
				return res.send(reply.E('An error occurred'));
	  	}) 
		})
	});

	// insert weather station into DB
	app.post('/api/weather_station/register', (req, res) => {
		db.weather_stations.findOne({
			where: {
				name: req.body.name
			}
		}).then(weather_station => {
			if (weather_station) {
				return res.send(reply.E('Station Name already exists'));
			}

			db.weather_stations.create({
				name: req.body.name,
				lat: req.body.lat,
				long: req.body.long,
				API_key: nanoid(50),
				created_by: req.user.uid
			}).then(station => {
				return res.send(reply.S('Station created.'));
			}).catch(err => {
				console.log(err);
				return res.send(reply.E('An error occurred'));
			})
		});
	});

	// insert turbine data into DB
	app.post('/api/turbine', (req, res) => {
		return res.send('Coming soon');
	});

	// insert turbine into DB
	app.post('/api/turbine/register', (req, res) => {
		return res.send('Coming soon');
	});

	app.get('/api/my_stations', (req, res) => {
		op.retrieveInfo(db.weather_stations, req.user.uid, (err, data) => {
			if (err) return res.send(reply.E('An error occurred'));
			if (data.length > 0) {
				return res.render('test', data);
			} else {
				return res.send(reply.E('No stations found'));
			}
		})
	})

	// retrieve weather data
	app.get('/api/weather_data', (req, res) => {
		db.weather_data.findAll({
			where: {
				station_id: req.query.station_id
			}
		}).then(data => {
		 	res.send(data);
		})
	});

	app.get('/api/temperature/monthly', (req, res) => {
		const params = {
			station_id: req.query.station_id,
			month: req.query.month,
			year: req.query.year
		};

		db.weather_data.findAll({
			attributes: [
				[sequelize.fn('ROUND', sequelize.fn('AVG', sequelize.col('temp')), 2), 'avg'],
				[sequelize.fn('MIN', sequelize.col('temp')), 'min'],
				[sequelize.fn('MAX', sequelize.col('temp')), 'max'],
				[sequelize.fn('DATE', sequelize.col('timestamp')), 'date']
			],
			where: {
				station_id: params.station_id,
				[sequelize.Op.and]: [
					{timestamp:
					sequelize.where(sequelize.fn('MONTH', sequelize.col('timestamp')), params.month)},
					{timestamp:
					sequelize.where(sequelize.fn('YEAR', sequelize.col('timestamp')), params.year)}
				]
			},
			group: sequelize.fn('DAY', sequelize.col('timestamp'))
		}).then(data => {
			return res.send(reply.S(data));
		}).catch(err => {
			console.log(err);
			return res.send(reply.E('An error occurred'));
		})
	});

	app.get('/api/wind/rose', (req, res) => {
		const params = {
			station_id: req.query.station_id,
			start: req.query.start,
			end: req.query.end
		};

		db.weather_data.findAll({
			attributes: [
				[sequelize.fn('ROUND', sequelize.fn('AVG', sequelize.col('wind_speed')), 2), 'speed_mean'],
				[sequelize.fn('ROUND', sequelize.fn('AVG', sequelize.col('wind_direction')), 2), 'direction_mean'],
				[sequelize.fn('DATE', sequelize.col('timestamp')), "date"],
				[sequelize.fn('HOUR', sequelize.col('timestamp')), "hour"]
			],
			where: {
				station_id: params.station_id,
				timestamp: {
					[sequelize.Op.gte]: params.start,
					[sequelize.Op.lte]: params.end
				}
			},
			group: [
				sequelize.fn('DAY', sequelize.col('timestamp')),
				sequelize.fn('HOUR', sequelize.col('timestamp'))
			]
		}).then(result => {
			const spawn = require('child_process').spawn;
			let data = JSON.stringify(result)
			const wind_rose = spawn('python3', ['app/winapi-python-modules/wind_rose.py', data]);
			var dataString = "";

			wind_rose.stdout.on('data', function(data){
				dataString += data.toString();
			wind_rose.stdout.on('end', function(){
				console.log(dataString);
				  return res.send(reply.S(JSON.parse(dataString))); 
				});
			});
		}).catch(err => {
			console.log(err);
			return res.send(reply.E('An error occurred'));
		})
	});

	/*
	 * import from file 
	 */
	
	/*app.get('/api/import', (req, res) => {
		const csv = require('fast-csv');
		const fs = require('fs');
		const FILEPATH = '/home/ture/winAPI/TEST.TXT';

		const stream = csv
	    .parseFile(FILEPATH, {headers: true, delimiter:'\t'})
	    .transform(data => ({
	    	station_id: 1,
	        timestamp: data.Date + ' ' + data.Time,
	        temp: (parseFloat(data.Temp1) + parseFloat(data.Temp2))/2,
	        humidity: parseInt(data.Humi),
	        pressure: parseInt(data.Pressure),
	        wind_speed: parseFloat(data.Speed),
	        wind_speed_min: parseFloat(data.Min),
	        wind_speed_max: parseFloat(data.Max),
	        wind_speed_std: parseFloat(data.StdAbw),
	        wind_direction: parseInt(data.Dir),
	        wind_direction_std: parseFloat(data.StdAbw2)

	    }))
	    .on('error', error => console.error(error))
	    .on('data', row => {
	    	db.weather_data.create(row);
	    	//console.log(JSON.stringify(row))
	    })
	    .on('end', rowCount => console.log(`Parsed ${rowCount} rows`));

		return res.send(reply.E('Read successful'));
	});*/
	
};