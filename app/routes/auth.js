// routes/user_routes.js

var authController = require('../controllers/authcontroller.js');

module.exports = function(app, passport) {
	app.get('/register', authController.register);
	app.post('/register', passport.authenticate('local-register', {
		successRedirect: '/dashboard',
		failureRedirect: '/register'
		}
	));

	app.get('/login', authController.login);
	app.post('/login', function(req, res, next) {
		passport.authenticate('local-login', function(err, user, info) {
			if (err) { return next(err); }
			if (!user) { return res.redirect('/login'); }
			req.logIn(user, function(err) {
				if (err) { return next(err); }
			return res.redirect('/dashboard');
			});
		})(req, res, next);
	});

	app.get('/logout', authController.logout);
};