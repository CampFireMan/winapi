
  //load bcrypt
var bCrypt = require('bcrypt');

module.exports = function(passport, user){

	var User = user;
	var LocalStrategy = require('passport-local').Strategy;


	passport.serializeUser((user, done) => {
		done(null, user.uid);
	});


	// used to deserialize the user
	passport.deserializeUser((id, done) => {
		User.findByPk(id).then(user => {
			if (user) {
				done(null, user.get());
			} else {
				done(user.errors,null);
			}
		});
	});


	passport.use('local-register', new LocalStrategy({           
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback : true
	}, (req, email, password, done) => {
			var generateHash = password => {
				return bCrypt.hashSync(password, bCrypt.genSaltSync(12), null);
			};

			User.findOne({where: {email:email}}).then(user => {
				if (user) {
					return done(null, false, {message : 'That email is already taken'} );
				} else {
					var userPassword = generateHash(password);
					var data = { 
					email:email,
					password:userPassword,
					first_name: req.body.first_name,
					last_name: req.body.last_name
				};

					User.create(data).then((newUser,created) => {
						if(!newUser){
							return done(null,false);
						}

						if(newUser){
							return done(null,newUser);
						}
					});
				}
			}); 
		}
	));

	//LOCAL Login
	passport.use('local-login', new LocalStrategy({
		usernameField : 'email',
		passwordField : 'password',
		passReqToCallback : true
	}, (req, email, password, done) => {
	var User = user;
	var isValidPassword = (userpass,password) => {
		return bCrypt.compareSync(password, userpass);
	}

	User.findOne({ where : { email: email}}).then(user => {
		if (!user) {
			return done(null, false, { message: 'Email does not exist' });
		}

		if (!isValidPassword(user.password,password)) {
			return done(null, false, { message: 'Incorrect password.' });
		}

		var userinfo = user.get();
		return done(null,userinfo);
		}).catch(err => {
			console.log("Error:",err);
			return done(null, false, { message: 'Something went wrong with your Login' });
		});
	}
	));
}
