//server.js

// options
const PORT = 80;
const sPORT = 443;

const fs = require('fs');
const path = require('path');
const hskey = fs.readFileSync('hacksparrow-key.pem');
const hscert = fs.readFileSync('hacksparrow-cert.pem')

const options = {
    key: hskey,
    cert: hscert
};

// environment
var env = require('dotenv').load();
var env_type = process.env.NODE_ENV;
var config = require(path.join(__dirname, 'config', 'config.json'))[env_type];

// loaded modules
const express = require('express');
const bodyParser = require('body-parser');

const passport = require('passport')
const session  = require('express-session')

const Sequelize = require('sequelize');
const sequelize = require('./config/db')(config, Sequelize);
const app = express();
const https = require('https');
const http = require('http');
const httpServer = http.createServer(app);
const httpsServer = https.createServer(options, app);
const exphbs = require('express-handlebars');
const favicon = require('serve-favicon');

// app settings
app.set('views', './views')
app.engine('hbs', exphbs({
    extname: '.hbs',
    defaultLayout: 'main',
    layoutsDir: './views/layouts',
    partialsDir: './views/partials'
}));
app.set('view engine', '.hbs');

app.use(bodyParser.urlencoded({extended: true}));

app.use(session({secret: 'keyboard cat',resave: true, saveUninitialized:true}));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, './views')));
app.use(favicon(path.join(__dirname, 'views', 'style', 'media', 'favicon.ico')));

app.enable('trust proxy');
app.use((req, res, next) => {
  if (req.secure) {
    next();
  } else {
    res.redirect('https://' + req.headers.host + req.url);
  }
});


// start server
sequelize
  .sync()
  .then(() => {
    console.log('Connection has been established successfully.');
    const db = require('./models')(fs, path, sequelize, Sequelize);
    require('./app/passport/passport.js')(passport, db.users);
    require('./app/routes')(app, Sequelize, db, passport);

    httpServer.listen(PORT);
    httpsServer.listen(sPORT, () => {

        console.log('We are live on ' + sPORT);
    });
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });