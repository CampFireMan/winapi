/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('weather_data', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    station_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: 'weather_stations',
        key: 'id'
      }
    },
    temp: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    humidity: {
      type: DataTypes.INTEGER(3),
      allowNull: true
    },
    pressure: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_speed: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_speed_min: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_speed_max: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_speed_std: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_direction: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_direction_std: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'weather_data'
  });
};
