module.exports = function(fs, path, sequelize, Sequelize) {
  var db = {};
  fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });
 
	Object.keys(db).forEach(function(modelName) {
	    if ("associate" in db[modelName]) {
	        db[modelName].associate(db);
	    }
	});

  return db;
}