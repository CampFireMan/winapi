/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('performance_data', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    turbine_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: 'turbines',
        key: 'id'
      }
    },
    power: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    wind_speed: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'performance_data'
  });
};
